using System;

namespace Evaluation.Horloge;
partial class Alarm
{
    public partial bool IsActive()
    {
        if (IsOneTime)
        {
            return DateTime.Now.Date == Time.Date && DateTime.Now.TimeOfDay >= Time.TimeOfDay;
        }
        else
        {
            return IsRecurring && Days.Contains(DateTime.Now.DayOfWeek.ToString().Substring(0, 2)) &&
                   DateTime.Now.TimeOfDay >= Time.TimeOfDay;
        }
    }

    public partial void UpdateNextOccurrence()
    {
        if (IsRecurring)
        {
            Time = Time.AddDays(7);
        }
        else
        {
            Time = DateTime.MaxValue;
        }
    }
}