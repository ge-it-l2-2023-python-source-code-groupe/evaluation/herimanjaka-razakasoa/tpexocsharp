using System;

namespace Evaluation.Horloge;
partial class Alarm
{
    public string Name { get; set; }
    public DateTime Time { get; set; }
    public string Days { get; set; }
    public bool IsOneTime { get; set; }
    public bool IsRecurring { get; set; }

    public Alarm(string name, DateTime time, string days, bool isOneTime, bool isRecurring)
    {
        Name = name;
        Time = time;
        Days = days;
        IsOneTime = isOneTime;
        IsRecurring = isRecurring;
    }
    public partial bool IsActive();
    public partial void UpdateNextOccurrence();
}