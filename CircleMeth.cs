using System;
namespace Evaluation.FormeGeometrique;
/// <summary>
/// Implemetation du corps des methodes partial
/// </summary>
partial class Circle
{

    public partial float GetPerimeter()
    {
        return Convert.ToSingle(2 * Rayon * Math.PI);
    }

    public partial float GetSurface()
    {
        return GetPerimeter() * Rayon / 2;
    }

    ///<summary>say if the point is on the Circle<summary>
    /// <returns>bool<returns>

    public partial bool IsInclude(Point p)
    {
        return Math.Pow(p.X, 2) + Math.Pow(p.Y, 2) <= Math.Pow(Rayon, 2);
    }


    public partial string Display()
    {
        return $"Circle({CirclePoint.X} ; {CirclePoint.Y} ; {Rayon})";
    }
}