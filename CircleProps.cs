namespace Evaluation.FormeGeometrique;
partial class Circle{
    
    public Point CirclePoint;
    public float Rayon{get;set;}

    public Circle(Point point,float rayon)
    {
        CirclePoint = point;
        Rayon = rayon;
    }

    /// <summary>
    /// Declaration des signature des methode partial
    /// </summary>
    public partial float GetPerimeter();
    public partial float GetSurface();
    public partial bool IsInclude(Point p);
    public partial string Display();
}