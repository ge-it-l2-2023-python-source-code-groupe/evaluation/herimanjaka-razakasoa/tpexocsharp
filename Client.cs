namespace Evaluation.Banque;
class Client
{
    //Propriete accessible en lecture seule dans une autre classe
    public string CIN { get; private set; }
    public string Nom { get; private set; }
    public string Prenom { get; private set; }
    public string Tel {get; set; }

    public Client()
    {
        CIN = default;
        Nom = default;
        Prenom = default;
        Tel = default;
    }
    // tel reste string vide par defaut si on ne le fournit pas, sinon il recoit le valeur 
    public Client(string cin, string nom , string prenom, string tel=""):
    this()
    {
        CIN = cin;
        Nom = nom;
        Prenom = prenom;
        Tel = tel;
    }
    
    public string Afficher(){
        return $@"
        Compte :
        ---------
        Nom : {Nom},
        Prenom : {Prenom},
        CIN : {CIN},
        Tel : {Tel}
        ";
    }
}