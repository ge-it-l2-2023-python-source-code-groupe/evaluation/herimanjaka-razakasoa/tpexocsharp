namespace Evaluation.Banque;
class Compte
{
    public static uint NombreDeCompte;
    public float Solde { get; private set; }
    public uint Code { get; private set; }
    public Client Proprietaire { get; set; }

    /// <summary>
    /// Constructeur static qui permet d'initialiser le membre static (code) pour le premier appel
    /// </summary>
    static Compte()
    {
        NombreDeCompte = 0;
    }

    public Compte(Client client, float montant) :
    this(client)
    {
        Solde = montant;
    }

    public Compte(Client client) :
    this()
    {
        Proprietaire = client;
    }

    public Compte()
    {
        Proprietaire = new();
        Solde = default;
        NombreDeCompte++;
        Code = NombreDeCompte;
    }

    public void Debiter(float montant, Compte compteClient)
    {
        Debiter(montant);
        compteClient.Crediter(montant);
    }

    public void Debiter(float montant)
    {
        Solde -= montant;
    }

    public void Crediter(float montant, Compte compteClient)
    {
        Crediter(montant);
        compteClient.Debiter(montant);
    }

    public void Crediter(float montant)
    {
        Solde += montant;
    }

    public string Resumer()
    {
        return $@"
        ************************
        Numéro de Compte: {Code}
        Solde de compte: {Solde}
        Propriétaire du compte:
        {Proprietaire.Afficher()}
        ************************";
    }

    public static string NombreCompteCree()
    {
        return $"Le nombre de comptes crées: {NombreDeCompte}";
    }
}