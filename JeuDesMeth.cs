using System;
namespace Evaluation.JeuDe;

partial class JeuDeDes
{

    public partial void CommencerJeu()
    {
        Console.WriteLine(@"
        **************************************

            Bienvenue dans le jeu de dés!

        **************************************

        ");

        Console.Write("Entrez le nombre de joueurs (maximum 3) : ");
        int nombreDeJoueurs = Int32.Parse(Console.ReadLine() ?? "1");

        Console.Write($"Entrez le nom du joueur 1 : ");
        string nomJoueur1 = Console.ReadLine() ?? "";
        joueur1 = new Joueur(nomJoueur1);

        if (nombreDeJoueurs > 1)
        {
            Console.Write($"Entrez le nom du joueur 2 : ");
            string nomJoueur2 = Console.ReadLine() ?? "";
            joueur2 = new Joueur(nomJoueur2);
        }

        if (nombreDeJoueurs > 2)
        {
            Console.Write($"Entrez le nom du joueur 3 : ");
            string nomJoueur3 = Console.ReadLine() ?? "";
            joueur3 = new Joueur(nomJoueur3);
        }

        while (true)
        {
            Console.Clear();
            Console.WriteLine(@"
            -------------------------------
                1- Nouvelle Manche
                2- Historique des Manches
                3- Quitter
            -------------------------------
            
            ");

            string choix = Console.ReadLine() ?? "";

            switch (choix)
            {
                case "1":
                    NouvelleManche();
                    break;
                case "2":
                    AfficherHistoriqueManches();
                    break;
                case "3":
                    return;
                default:
                    Console.WriteLine("Option invalide. Veuillez réessayer.");
                    break;
            }
        }
    }

    public partial void NouvelleManche()
    {
        Console.Clear();
        Int32 scoreJoueur1 = LanceLeDe(joueur1.Nom);
        Int32 scoreJoueur2 = joueur2 != null ? LanceLeDe(joueur2.Nom) : 0;
        Int32 scoreJoueur3 = joueur3 != null ? LanceLeDe(joueur3.Nom) : 0;

        AfficherResultatManche(scoreJoueur1, scoreJoueur2, scoreJoueur3);

        historiqueManches += $"{DateTime.Now};{scoreJoueur1}";

        if (joueur2 != null)
        {
            historiqueManches += $";{scoreJoueur2}";
        }

        if (joueur3 != null)
        {
            historiqueManches += $";{scoreJoueur3}";
        }

        historiqueManches += "|";

        Console.WriteLine("Appuyez sur une touche pour continuer...");
        Console.ReadKey();
    }

    public partial void AfficherResultatManche(int scoreJoueur1, int scoreJoueur2, int scoreJoueur3)
    {
        Console.WriteLine($"{joueur1.Nom} a lancé le dé : {scoreJoueur1}");

        if (joueur2 != null)
        {
            Console.WriteLine($"{joueur2.Nom} a lancé le dé : {scoreJoueur2}");
        }

        if (joueur3 != null)
        {
            Console.WriteLine($"{joueur3.Nom} a lancé le dé : {scoreJoueur3}");
        }

        Console.WriteLine("Appuyez sur une touche pour voir le résultat...");
        Console.ReadKey();
    }

    public partial void AfficherHistoriqueManches()
    {
        Console.Clear();

        Console.WriteLine("Historique des manches :");

        foreach (var manche in historiqueManches.Split('|', StringSplitOptions.RemoveEmptyEntries))
        {

            Console.WriteLine($"Date et heure : {manche.Split(';')[0]}");

            Console.WriteLine($"{joueur1.Nom} - {manche.Split(';')[1]} points");

            if (joueur2 != null)
            {
                Console.WriteLine($"{joueur2.Nom} - {manche.Split(';')[2]} points");
            }

            if (joueur3 != null)
            {
                Console.WriteLine($"{joueur3.Nom} - {manche.Split(';')[3]} points");
            }

            Console.WriteLine();
        }

        Console.WriteLine("Appuyez sur une touche pour revenir au menu principal...");
        Console.ReadKey();
    }

    public partial Int32 LanceLeDe(string nom)
    {
        Console.WriteLine($"Tour de {nom} Voulez-vous lancer le de ? [y,n]");
        if(Console.ReadLine() == "y")
            return random.Next(1, 7);
        else
        {
            Console.WriteLine("Son tour est passe");
            return 0;
        }
    }
}