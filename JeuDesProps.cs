using System;
namespace Evaluation.JeuDe;

partial class JeuDeDes
{
    private static Random random = new Random();
    private Joueur joueur1;
    private Joueur joueur2;
    private Joueur joueur3;
    private string historiqueManches = "";

    public partial void CommencerJeu();
    public partial void NouvelleManche();
    public partial void AfficherResultatManche(int scoreJoueur1, int scoreJoueur2, int scoreJoueur3);
    public partial void AfficherHistoriqueManches();
    public partial Int32 LanceLeDe(string nom);

}


