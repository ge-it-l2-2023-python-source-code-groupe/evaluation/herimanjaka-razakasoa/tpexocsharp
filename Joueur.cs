namespace Evaluation.JeuDe;
class Joueur
{
        public string Nom { get; }
        public int Score { get; set; }

        public Joueur(string nom):
        this()
        {
            Nom = nom;
            Score = 0;
        }
        public Joueur(){ }
}