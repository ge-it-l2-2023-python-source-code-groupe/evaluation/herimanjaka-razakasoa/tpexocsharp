namespace Evaluation.FormeGeometrique;
partial class Point
{
    /// <summary>
    /// Implementation du corps du methode partiel display en renvoyant un string qui indique le coordonne
    /// </summary>
    /// <returns>string Coordonnes du point</returns>
    public partial string Display()
    {
        return $"Point({X} ; {Y})";
    }
}