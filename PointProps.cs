namespace Evaluation.FormeGeometrique;
partial class Point{
    public float X{get;set;}
    public float Y{get;set;}

    public Point(float x, float y){
        X = x;
        Y = y;
    }

    /// <summary>
    /// Declaration du signature de la methode partiel
    /// </summary>
    public partial string Display();
}