﻿using System;
namespace TpExoCsharp;

using System.Collections.Generic;
using Evaluation.Banque;
using Evaluation.FormeGeometrique;
using Evaluation.JeuDe;
using Evaluation.Horloge;
using Microsoft.VisualBasic;

class Program
{
    static void Main(string[] args)
    {
        while (true)
        {
            Console.Clear();
            Separator(45);
            Console.WriteLine($@"

        Bienvenue, Ci-dessous les listes des tp:
        ---------------------------------

        1-  TP numero 1 Client & Compte Class
        2-  TP numero 2 Circle & Point Class
        3-  TP numero 3 Le jeu de dés
        4-  TP numero 4 Horloge module
        5-  Quitter le programme
    ");
            Separator(45);

            Console.WriteLine("\nEntrez votre choix : ");
            string choice = Console.ReadLine() ?? "";
            Console.Clear();
            switch (choice)
            {
                case "1":
                    String cin, lastName, firstName, tel;
                    Single somme;

                    #region Create Compte Client 1

                    Console.Write($"Compte 1:\nDonner Le CIN: ");
                    cin = Console.ReadLine() ?? "";
                    Console.Write($"Donner Le Nom: ");
                    lastName = Console.ReadLine() ?? "";
                    Console.Write($"Donner Le Prénom: ");
                    firstName = Console.ReadLine() ?? "";
                    Console.Write($"Donner Le numéro de télephone: ");
                    tel = Console.ReadLine() ?? "";

                    Compte cpt1 = new Compte(new Client(cin, firstName, lastName, tel));
                    Console.WriteLine($"Détails du compte:{cpt1.Resumer()}");

                    #endregion
                    #region Crediter & Debiter Compte Client 1

                    //Crediter cpt 1
                    Console.Write($"Donner le montant à déposer: ");
                    somme = Convert.ToSingle(Console.ReadLine() ?? "0.0f");
                    cpt1.Crediter((float)somme);
                    Console.WriteLine($"Opération bien effectuée{cpt1.Resumer()}");
                    //Debiter cpt 1
                    Console.Write($"Donner le montant à retirer: ");
                    somme = Convert.ToSingle(Console.ReadLine() ?? "0.0f");
                    cpt1.Debiter((float)somme);
                    Console.WriteLine($"Opération bien effectuée{cpt1.Resumer()}");

                    #endregion
                    #region Create Compte Client 2

                    Console.Write($"\n\n\nCompte 2:\nDonner Le CIN: ");
                    cin = Console.ReadLine() ?? "";
                    Console.Write($"Donner Le Nom: ");
                    lastName = Console.ReadLine() ?? "";
                    Console.Write($"Donner Le Prénom: ");
                    firstName = Console.ReadLine() ?? "";
                    Console.Write($"Donner Le numéro de télephone: ");
                    tel = Console.ReadLine() ?? "";

                    Compte cpt2 = new Compte(new Client(cin, firstName, lastName, tel));
                    Console.WriteLine($"Détails du compte:{cpt2.Resumer()}");

                    #endregion
                    #region Crediter Compte Client 2 from Compte Client 1 & Debiter Compte Client 1 from Compte Client 2

                    //Crediter cpt2 -> cpt1
                    Console.WriteLine($"Crediter le compte {cpt2.Code} à partir du compte {cpt1.Code}");
                    Console.Write($"Donner le montant à déposer: ");
                    somme = Convert.ToSingle(Console.ReadLine() ?? "0.0f");
                    cpt2.Crediter((float)somme, cpt1);
                    Console.WriteLine($"Opération bien effectuée");
                    //Debiter cpt1 -> cpt2
                    Console.WriteLine($"Débiter le compte {cpt1.Code} et créditer le compte {cpt2.Code}");
                    Console.Write($"Donner le montant à retirer: ");
                    somme = Convert.ToSingle(Console.ReadLine() ?? "0.0f");
                    cpt1.Debiter((float)somme, cpt2);
                    Console.Write($"Opération bien effectuée");

                    #endregion

                    Console.WriteLine($"{cpt1.Resumer()}{cpt2.Resumer()}");
                    Console.Write($"\n\n\n{Compte.NombreCompteCree()}");

                    Separator(45);
                    FinChoix();
                    break;

                case "2":
                    // Code pour le TP numero 2
                    Separator(45);
                    Console.WriteLine(@"
                    Tp numero 2 : 
                        - Creer un Cercle a partir d'un point et un rayon
                        - Creer un point et voir si c'est inclus dans le cercle

                    ");
                    Separator(45);

                    #region Demande d'info pou le premier point pour former le cercle
                    Console.WriteLine("Entrez l'abcisse du point :");
                    float x = float.Parse(Console.ReadLine() ?? "0.0f");

                    Console.WriteLine("Entrez l'ordonne du point :");
                    float y = float.Parse(Console.ReadLine() ?? "0.0f");

                    Console.WriteLine("Entrez le Rayon du Cercle :");
                    float r = float.Parse(Console.ReadLine() ?? "0.0f");

                    Circle YourCircle = new(new Point(x, y), r);
                    Console.WriteLine(YourCircle.Display());
                    Console.WriteLine($"The perimeter of your circle is : {YourCircle.GetPerimeter()}");
                    Console.WriteLine($"The surface of your circle is : {YourCircle.GetSurface()}");
                    Console.WriteLine();

                    #endregion


                    #region Demande d'info pou le deuxieme point a creer
                    Console.WriteLine("Entrez l'abcisse d'un autre point :");
                    x = float.Parse(Console.ReadLine() ?? "0.0f");

                    Console.WriteLine("Entrez l'ordonne d'un autre point :");
                    y = float.Parse(Console.ReadLine() ?? "0.0f");

                    Point YourPoint = new(x, y);
                    Console.WriteLine(YourPoint.Display());

                    if (YourCircle.IsInclude(YourPoint))
                        Console.WriteLine("Cette dernier est inclus dans le Cercle");
                    else
                        Console.WriteLine("Cette dernier est inclus dans le Cercle");

                    #endregion

                    FinChoix();
                    break;

                case "3":
                    // Code pour le TP numero 3
                    JeuDeDes jeu = new();
                    jeu.CommencerJeu();
                    FinChoix();
                    break;
                case "4":
                    // Code pour le TP numero 4
                    bool horlogeTp = true;
                    while (horlogeTp)
                    {
                        Console.Clear();
                        ShowMainMenu();
                        string choix = Console.ReadLine() ?? "";

                        switch (choix)
                        {
                            case "1":
                                ShowAlarmMenu();
                                break;
                            case "2":
                                ShowWorldClockMenu();
                                break;
                            case "3":
                                horlogeTp = false;
                                break;
                            default:
                                Console.WriteLine("Choix non valide. Veuillez réessayer.");
                                break;
                        }
                    }

                    break;

                case "5":
                    return;
                default:
                    Console.WriteLine("Erreur de saisi ou choix non dispo");
                    FinChoix();
                    break;
            }
        }


    }

    #region delaration des methode utilitaires
    static void Separator(int taille)
    {
        Console.WriteLine();
        for (int i = 0; i < taille; i++)
            Console.Write("_");
    }
    static void FinChoix()
    {
        Console.WriteLine(@"

        ----------------------------------------------------
                Taper Entrer pour voir d'autre TP
                Sinon taper q pour Quitter
        ----------------------------------------------------
                    ");
        if (Console.ReadLine() == "q")
            return;
    }
    #endregion

    #region declaration des methode utilise dans le TP 4
    static List<Alarm> alarms = new();
    static List<WorldClock> worldClocks = new();
    static void ShowMainMenu()
    {
        Console.WriteLine(@"
        ***********************************************
        Bienvenue dans le menu principale
            Choisissez une option :
            1 - Alarme
            2 - Horloge
            3 - Retourner au menu des TP

        ********************************************
");
        Console.Write("Quel est votre choix ?");
    }


    static void ShowAlarmMenu()
    {
        Console.Clear();
        Console.WriteLine(@"
        ***********************************************
            Choisissez une option :
            1 - Voir les alarmes actif
            2 - Creer une Alarme
            3 - Retourner au menu precedent

        ********************************************
");

        Console.Write("Quel est votre choix ?");
        string choice = Console.ReadLine() ?? "";

        switch (choice)
        {
            case "1":
                ShowAlarms();
                break;
            case "2":
                CreateAlarm();
                break;
            case "3":
                break;
            default:
                Console.WriteLine("Choix non valide. Veuillez réessayer.");
                break;
        }
        Console.WriteLine(@"
        ______________________________________________________
            Taper entrez pour rester au menu alarm
            Sinon tapez q pour retourner au menu principale
        _______________________________________________________
        ");
        if (Console.ReadLine() == "q")
            return;
        else
            ShowAlarmMenu();
    }

    static void ShowAlarms()
    {
        bool isVide = true;
        Console.WriteLine("\nListe des alarmes actives :");
        foreach (var alarm in alarms)
        {
            isVide = false;
            Console.WriteLine($"{alarm.Name} - {alarm.Time} ({(alarm.IsRecurring ? "périodique" : "unique")})");
            if (alarm.IsRecurring)
            {
                Console.WriteLine(alarm.Days);
            }
        }
        if (isVide)
            Console.WriteLine("\tAucun alarme n'est defini pour le moment");
    }

    static void CreateAlarm()
    {
        Console.WriteLine(@"
        *******************************
        Info de la nouvelle Alarme
        ******************************
        ");
        Console.Write("Donnez un nom pour cette nouvelle Alarme : ");
        string name = Console.ReadLine() ?? "";

        Console.Write("Heure de l’alarme (hh:mm) : ");
        DateTime time;
        while (!DateTime.TryParse(Console.ReadLine(), out time))
        {
            Console.Write("Format d'heure invalide. Réessayez : ");
        }

        Console.Write("Date de planification (L/M/ME/J/V/S/D) : ");
        string days = Console.ReadLine() ?? "";

        Console.Write("Lancer une seule fois (o/n) : ");
        bool isOneTime = (Console.ReadLine() ?? "").ToLower() == "o";

        Console.Write("Périodique (o/n) : ");
        bool isRecurring = (Console.ReadLine() ?? "").ToLower() == "o";

        Alarm newAlarm = new(name, time, days, isOneTime, isRecurring);
        alarms.Add(newAlarm);

        Console.WriteLine("Votre nouvelle alarme est enregistrée avec succes :)");
    }
    static void ShowWorldClockMenu()
    {
        Console.Clear();

        Console.WriteLine($@"
        Antananarivo
        {DateAndTime.Now}

        (Horloge Menu) Choisissez une option :
         --------------------------------------

        1 - Voir les Horloge actif
        2 - Ajouter une horloge
        3 - Retour menu principale de cette tp
        _______________________________________
        ");

        Console.Write("Quel est votre choix ? ");

        string choice = Console.ReadLine() ?? "";
        switch (choice)
        {
            case "1":
                ShowWorldClocks();
                break;
            case "2":
                AddWorldClock();
                break;
            case "3":
                break;
            default:
                Console.WriteLine("Choix non valide. Veuillez réessayer.");
                break;
        }
        Console.WriteLine(@"
        ______________________________________________________
            Taper entrez pour rester au menu Horloges
            Sinon tapez q pour retourner au menu principale
        _______________________________________________________
        ");
        if (Console.ReadLine() == "q")
            return;
        else
            ShowWorldClockMenu();
    }
    static void ShowWorldClocks()
    {
        Console.WriteLine("\nListe des Horloges :");
        bool isVide = true;
        foreach (var clock in worldClocks)
        {
            Console.WriteLine(clock);
            isVide = false;
        }
        if (isVide)
            Console.WriteLine("\tAucun n'est defini pour le moment.");
    }

    static void AddWorldClock()
    {
        Console.WriteLine("\nChoisissez une ville : [Moscou, Dubai, Mexique]");
        string cityName = Console.ReadLine() ?? "";

        int timeDifference;

        //convertir en lowercase pour que Moscou ou moscou soit le meme (exemple)
        switch (cityName.ToLower())
        {
            case "moscou":
                timeDifference = 3;
                break;
            case "dubai":
                timeDifference = 4;
                break;
            case "mexique":
                timeDifference = -6;
                break;
            default:
                Console.WriteLine("Cette ville n'est pas disponible pour le moment.");
                return;
        }

        WorldClock newClock = new(cityName, timeDifference);
        worldClocks.Add(newClock);

        Console.WriteLine("Votre Horloge a bien été enregistrée.");
    }

    #endregion

}
