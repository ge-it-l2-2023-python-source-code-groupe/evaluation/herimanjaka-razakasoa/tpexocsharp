using System;

class WorldClock
{
    public string CityName { get; }
    public int TimeDifference { get; }

    public WorldClock(string cityName, int timeDifference)
    {
        CityName = cityName;
        TimeDifference = timeDifference;
    }
    /// <summary>
    /// Redefinir le methode ToString dans Object pour adapter l'affichage selon nos besoin
    /// </summary>
    public override string ToString()
    {
        DateTime cityTime = DateTime.UtcNow.AddHours(TimeDifference);
        return $"{CityName} - {cityTime.ToString("HH:mm")} {(TimeDifference >= 0 ? "+" : "")}{TimeDifference}h";
    }
}